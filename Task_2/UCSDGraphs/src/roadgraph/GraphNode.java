/**
 * @author nikmac
 */
package roadgraph;

import java.util.ArrayList;
import java.util.List;

import geography.GeographicPoint;

/**
 * @author nikmac
 *
 * This class represents a node of the graph
 */
public class GraphNode {
	private GeographicPoint location;
	private String name;
	private List<GraphEdge> edges;
	private List<GraphNode> neighbors;
	
	/**
	 * Private constructor
	 * A GraphNode can be created only through the nested builder class
	 */
	private GraphNode(String n, GeographicPoint loc) {
		this.edges = new ArrayList<GraphEdge>();
		this.neighbors = new ArrayList<GraphNode>();
		this.name = n;
		this.location = loc;
	}
	
	/**
	 * Set the name
	 */
	public void setName(String n) { this.name = n; }
	
	/**
	 * Set the location
	 */
	public void setLocation(GeographicPoint loc) { this.location = loc; }
	
	/**
	 * Add the contents of ed to the edges' list
	 */
	public void setEdges(List<GraphEdge> ed) { this.edges.addAll(ed); }
	
	/**
	 * Add the contents of gn to the neighbors' list
	 */
	public void setNeighbors(List<GraphNode> gn) { this.neighbors.addAll(gn); }
	
	/**
	 * Add ed to the edges' list
	 */
	public void addEdge(GraphEdge ed) {	this.edges.add(ed); }
	
	/**
	 * Add gn to the neighbors' list
	 */
	public void addNeighbor(GraphNode gn) { this.neighbors.add(gn); }
	
	/**
	 * Attempt o delete an edge from the edges' list
	 * @return true if the edge was deleted / false if the edge was not in the list
	 */
	public boolean deleteEdge(GraphEdge ed) { return this.edges.remove(ed); }
	
	/**
	 * Attempt o delete a node from the neighbors' list
	 * @return true if the node was deleted / false if the node was not in the list
	 */
	public boolean deleteNeighbor(GraphNode gn) { return this.neighbors.remove(gn); }
	
	/**
	 * @return the location of the node as a GeographicPointInstance
	 */
	public GeographicPoint getLocation() { return this.location; }
	
	/**
	 * @return the name of the node
	 */
	public String getName() { return this.name; }
	
	/**
	 * @return a list with the out neighbors of this node
	 */
	public List<GraphNode> getNeighbors() { return this.neighbors; }
	
	/**
	 * @return a list with the out edges of this node
	 */
	public List<GraphEdge> getEdges() { return this.edges; }
	
	
	
	
	
	/** 
	 * This nested class will be used to construct GraphNodes.
	 * It is an implementation of the "Builder" pattern to
	 * allow for optional parameters. It's static because all
	 * nodes only need one builder.
	 */
	public static class GraphNodeBuilder {
		
		private GeographicPoint _location;
		private String _name;
		
		/** 
		 * Default public constructor
		 */
		public GraphNodeBuilder() {	
			this._location = new GeographicPoint(0, 0);
			this._name = "None_Defined";
		}
		
		/** 
		 * Change the default name
		 */
		public GraphNodeBuilder name(String n) { 
			this._name = n;
			return this;
		}
		
		/** 
		 * Change the default location
		 */
		public GraphNodeBuilder location(GeographicPoint loc) {
			this._location = loc;
			return this;
		}
		
		/** 
		 * Create the GraphNode instance with the builder's parameters
		 */
		public GraphNode build() {
			return new GraphNode(this._name, this._location);
		}
		
	}
	
}
