/**
 * @author nikmac
 */
package roadgraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import geography.GeographicPoint;

/**
 * @author nikmac
 *
 * This class represents a node of the graph
 */
public class GraphNode {//implements Comparable<GraphNode>{
	private GeographicPoint location;
	private String name;
	private HashMap<GraphNode, GraphEdge> edges;
	private List<GraphNode> neighbors;
	/**
	 * the distance attribute is only used as a weight
	 * for the Dijkstra and A* algorithms
	 * it is the distance of this node to the starting node
	 * defined in the search when it runs
	 */
	private double distance; 
	
	/**
	 * Private constructor
	 * A GraphNode can be created only through the nested builder class
	 */
	private GraphNode(String n, GeographicPoint loc) {
		this.edges = new HashMap<GraphNode, GraphEdge>();
		this.neighbors = new ArrayList<GraphNode>();
		this.name = n;
		this.location = loc;
		this.initDistance();
	}
	
	/**
	 * 
	 * print the node location
	 */
	public void printNode() {
		System.out.println("Node: " + this.getLocation().getX() + ", " + this.getLocation().getY());
	}
	/**
	 * Set the name
	 */
	public void setName(String n) { this.name = n; }
	
	/**
	 * Set the location
	 */
	public void setLocation(GeographicPoint loc) { this.location = loc; }
	
	/**
	 * Set the distance
	 */
	public void setDistance(double d) { this.distance = d; }
	
	/**
	 * Initialize the distance
	 */
	public void initDistance() { this.distance = Double.POSITIVE_INFINITY; }
	
	/**
	 * Add the contents of ed to the edges' list
	 */
	public void setEdges(HashMap<GraphNode, GraphEdge> ed) { this.edges.putAll(ed); }
	
	/**
	 * Add the contents of gn to the neighbors' list
	 */
	public void setNeighbors(List<GraphNode> gn) { this.neighbors.addAll(gn); }
	
	/**
	 * Add ed to the edges' list
	 */
	public void addEdge(GraphEdge ed, GraphNode nd) {	this.edges.put(nd, ed); }
	
	/**
	 * Add gn to the neighbors' list
	 */
	public void addNeighbor(GraphNode gn) { this.neighbors.add(gn); }
	
	/**
	 * Attempt o delete an edge from the edges' list
	 * @return true if the edge was deleted / false if the edge was not in the list
	 */
	public boolean deleteEdge(GraphEdge ed, GraphNode nd) { return this.edges.remove(nd, ed); }
	
	/**
	 * Attempt o delete a node from the neighbors' list
	 * @return true if the node was deleted / false if the node was not in the list
	 */
	public boolean deleteNeighbor(GraphNode gn) { return this.neighbors.remove(gn); }
	
	/**
	 * @return the location of the node as a GeographicPointInstance
	 */
	public GeographicPoint getLocation() { 
		return new GeographicPoint(this.location.getX(), this.location.getY()); 
	}
	
	/**
	 * @return the name of the node
	 */
	public String getName() { return this.name; }
	
	/**
	 * @return the distance
	 */
	public double getDistance() { return this.distance; }
	
	/**
	 * @return a list with the out neighbors of this node
	 */
	public List<GraphNode> getNeighbors() { 
		return new ArrayList<GraphNode>(this.neighbors);
	}
	
	/**
	 * @return a list with the out edges of this node
	 */
	public HashMap<GraphNode, GraphEdge> getEdges() { 
		return new HashMap<GraphNode, GraphEdge>(this.edges); 
	}
	
	/**
	 * 
	 * @param GraphEdge end
	 * @return the edge starting from this and ending at end
	 */
	public GraphEdge getNeighborEdge(GraphNode nd) {
		return this.edges.get(nd);
	}
	
	
	/**
	 * @param o
	 * @return -1, 0, 1 when current node distance is <, ==, or > than 
	 * o node
	 */
	/*
	@Override
	public int compareTo(GraphNode o) {
        if (this.distance == o.getDistance())
            return 0;
        else if (this.distance > o.getDistance())
            return 1;
        else
            return -1;
	}
	*/
	
	/** 
	 * This nested class will be used to construct GraphNodes.
	 * It is an implementation of the "Builder" pattern to
	 * allow for optional parameters. It's static because all
	 * nodes only need one builder.
	 */
	public static class GraphNodeBuilder {
		
		private GeographicPoint _location;
		private String _name;
		
		/** 
		 * Default public constructor
		 */
		public GraphNodeBuilder() {	
			this._location = new GeographicPoint(0, 0);
			this._name = "None_Defined";
		}
		
		/** 
		 * Change the default name
		 */
		public GraphNodeBuilder name(String n) { 
			this._name = n;
			return this;
		}
		
		/** 
		 * Change the default location
		 */
		public GraphNodeBuilder location(GeographicPoint loc) {
			this._location = loc;
			return this;
		}
		
		/** 
		 * Create the GraphNode instance with the builder's parameters
		 */
		public GraphNode build() {
			return new GraphNode(this._name, this._location);
		}
		
	}
}
