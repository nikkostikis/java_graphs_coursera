/**
 * @author UCSD MOOC development team and YOU
 * 
 * A class which reprsents a graph of geographic locations
 * Nodes in the graph are intersections between 
 *
 */
package roadgraph;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.function.Consumer;


import geography.GeographicPoint;
import util.GraphLoader;


/**
 * @author UCSD MOOC development team and YOU
 * 
 * A class which represents a graph of geographic locations
 * Nodes in the graph are intersections between 
 *
 */
public class MapGraph {
	//TODO: Add your member variables here in WEEK 2
	private Map<GeographicPoint, GraphNode> nodes;
	private List<GraphEdge> edges;
	
	
	/** 
	 * Create a new empty MapGraph 
	 */
	public MapGraph()
	{
		// TODO: Implement in this constructor in WEEK 2
		nodes = new HashMap<GeographicPoint, GraphNode>();
		edges = new ArrayList<GraphEdge>();
	}
	
	/**
	 * Get the number of vertices (road intersections) in the graph
	 * @return The number of vertices in the graph.
	 */
	public int getNumVertices()
	{
		//TODO: Implement this method in WEEK 2
		return nodes.size();
	}
	
	/**
	 * Return the intersections, which are the vertices in this graph.
	 * @return The vertices in this graph as GeographicPoints - as a new Collection to 
	 * avoid exposure of internal structure to public interface
	 */
	public Set<GeographicPoint> getVertices()
	{
		//TODO: Implement this method in WEEK 2
		return (new HashSet<GeographicPoint>(nodes.keySet()));
	}
	
	/**
	 * Get the number of road segments in the graph
	 * @return The number of edges in the graph.
	 */
	public int getNumEdges()
	{
		//TODO: Implement this method in WEEK 2
		return edges.size();
	}

	
	/** Add a node corresponding to an intersection at a Geographic Point
	 * If the location is already in the graph or null, this method does 
	 * not change the graph.
	 * @param location  The location of the intersection
	 * @return true if a node was added, false if it was not (the node
	 * was already in the graph, or the parameter is null).
	 */
	public boolean addVertex(GeographicPoint location)
	{
		// TODO: Implement this method in WEEK 2
		
		//if the location is not null and it is not already mapped inside nodes...
		if ( (location != null) && !this.nodes.containsKey(location) ) {
			//create a new node using its builder
			GraphNode nd = new GraphNode.GraphNodeBuilder()
					.location(location)
					.build();
			//put the new node in the nodes HashMap
			this.nodes.put(location, nd);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Adds a directed edge to the graph from pt1 to pt2.  
	 * Precondition: Both GeographicPoints have already been added to the graph
	 * @param from The starting point of the edge
	 * @param to The ending point of the edge
	 * @param roadName The name of the road
	 * @param roadType The type of the road
	 * @param length The length of the road, in km
	 * @throws IllegalArgumentException If the points have not already been
	 *   added as nodes to the graph, if any of the arguments is null,
	 *   or if the length is less than 0.
	 */
	public void addEdge(GeographicPoint from, GeographicPoint to, String roadName,
			String roadType, double length) throws IllegalArgumentException {

		//TODO: Implement this method in WEEK 2
		if ( (from == null) || (to == null) || 
				(roadName == null) || (roadType == null) || 
				(!this.nodes.containsKey(from)) || (!this.nodes.containsKey(to)) || 
				(length == 0) ) { 
			throw new IllegalArgumentException("I caught an IllegalArgumentException from MapGraph.addEdge!");
		}
		//create a new edge using its builder and the parameters from above
		GraphEdge ge = new GraphEdge.GraphEdgeBuilder()
				.startLoc(from)
				.endLoc(to)
				.name(roadName)
				.type(roadType)
				.length(length)
				.build();
		//pass on the edge to be added to the list
		this.addActualEdge(ge);
	}
	
	/**
	 * Adds a GraphEdge object to the edges list.
	 * It calls a notifier (notifyNode) to update the from node of its new
	 * neighbor and edge
	 * It is only called by the addEdge method so no 
	 * preconditions will be violated
	 * @param edge
	 */
	private void addActualEdge(GraphEdge edge) {
		this.edges.add(edge);
		
		//notify the from node for the new edge
		this.notifyNode(edge);
	}
	
	/**
	 * Notifies the from node of an edge about its
	 * new neighbor and new edge. 
	 * The edge is passed on from addActualEdge
	 * so the from and to nodes are already in the nodes HashMap.
	 * There is no need to check anything.
	 * @param edge
	 */
	private void notifyNode(GraphEdge edge) {
		//add the edge to the from node's edges list
		this.nodes.get(edge.getStart()).addEdge(edge, this.nodes.get(edge.getEnd()));
		
		//add the edge's to node to the edge's from node's neighbors list
		this.nodes.get(edge.getStart()).addNeighbor(this.nodes.get(edge.getEnd()));
	}
	
	/**
	 * This method prints the nodes and the edges of the MapGraph.
	 * It is used for testing
	 */
	public void printGraph() {
		for (GeographicPoint gp : this.nodes.keySet()) {
			System.out.println("Node Location: " + this.nodes.get(gp).getName() + " : "
					+ this.nodes.get(gp).getLocation().getX() + ", " 
					+ this.nodes.get(gp).getLocation().getY() );
		}
		for (GraphEdge ge : this.edges) {
			System.out.println("Edge Name: " + ge.getName());
		}
	}

	/** Find the path from start to goal using breadth first search
	 * 
	 * @param start The starting location
	 * @param goal The goal location
	 * @return The list of intersections that form the shortest (unweighted)
	 *   path from start to goal (including both start and goal).
	 */
	public List<GeographicPoint> bfs(GeographicPoint start, GeographicPoint goal) {
		// Dummy variable for calling the search algorithms
        Consumer<GeographicPoint> temp = (x) -> {};
        return bfs(start, goal, temp);
	}
	
	/** Find the path from start to goal using breadth first search
	 * 
	 * @param start The starting location
	 * @param goal The goal location
	 * @param nodeSearched A hook for visualization.  See assignment instructions for how to use it.
	 * @return The list of intersections that form the shortest (unweighted)
	 *   path from start to goal (including both start and goal).
	 */
	public List<GeographicPoint> bfs(GeographicPoint start, 
			 					     GeographicPoint goal, Consumer<GeographicPoint> nodeSearched)
	{
		// TODO: Implement this method in WEEK 2
		
		GraphNode gnstart = this.nodes.get(start);
		GraphNode gngoal = this.nodes.get(goal);
		if ( gnstart == null || gngoal == null ) {
			System.out.println("Start or goal node is null!  No path exists.");
			return null;
		}

		HashSet<GraphNode> visited = new HashSet<GraphNode>();
		Queue<GraphNode> toExplore = new LinkedList<GraphNode>();
		HashMap<GraphNode, GraphNode> parentMap = new HashMap<GraphNode, GraphNode>();
		toExplore.add(gnstart);
		boolean found = false;
		while (!toExplore.isEmpty()) {
			GraphNode curr = toExplore.remove();
			// Hook for visualization.  See writeup.
			nodeSearched.accept(curr.getLocation());
			if (curr == gngoal) {
				found = true;
				break;
			}
			List<GraphNode> neighbors = curr.getNeighbors();
			ListIterator<GraphNode> it = neighbors.listIterator(neighbors.size());
			while (it.hasPrevious()) {
				GraphNode next = it.previous();

				if (!visited.contains(next)) {
					visited.add(next);
					parentMap.put(next, curr);
					toExplore.add(next);
					// Hook for visualization.  See writeup.
					nodeSearched.accept(next.getLocation());
				}
			}
		}
		
		if (!found) {
			System.out.println("No path exists");
			return null;
		}
		
		// return the reconstructed path
		return this.reconstructPath(gnstart, gngoal, parentMap);
		
	}
	
	/**
	 * Reconstructs the path from gnstart to gngoal using the parentMap.
	 * Called by bfs method
	 * @param gnstart
	 * @param gngoal
	 * @param parentMap
	 * @return a List of GeographicPoints
	 */
	private List<GeographicPoint> reconstructPath(GraphNode gnstart, GraphNode gngoal, HashMap<GraphNode, GraphNode> parentMap) {
		LinkedList<GeographicPoint> path = new LinkedList<GeographicPoint>();
		GraphNode curr = gngoal;
		while (curr != gnstart) {
			path.addFirst(curr.getLocation());
			curr = parentMap.get(curr);
		}
		path.addFirst(gnstart.getLocation());
		
		return path;
	}

	/** Find the path from start to goal using Dijkstra's algorithm
	 * 
	 * @param start The starting location
	 * @param goal The goal location
	 * @return The list of intersections that form the shortest path from 
	 *   start to goal (including both start and goal).
	 */
	public List<GeographicPoint> dijkstra(GeographicPoint start, GeographicPoint goal) {
		// Dummy variable for calling the search algorithms
		// You do not need to change this method.
        Consumer<GeographicPoint> temp = (x) -> {};
        return dijkstra(start, goal, temp);
	}
	
	/** Find the path from start to goal using Dijkstra's algorithm
	 * 
	 * @param start The starting location
	 * @param goal The goal location
	 * @param nodeSearched A hook for visualization.  See assignment instructions for how to use it.
	 * @return The list of intersections that form the shortest path from 
	 *   start to goal (including both start and goal).
	 */
	public List<GeographicPoint> dijkstra(GeographicPoint start, 
										  GeographicPoint goal, Consumer<GeographicPoint> nodeSearched)
	{
		// TODO: Implement this method in WEEK 3
		
		//Define the gnstart and gngoal or break
		GraphNode gnstart = this.nodes.get(start);
		GraphNode gngoal = this.nodes.get(goal);
		if ( gnstart == null || gngoal == null ) {
			System.out.println("Start or goal node is null!  No path exists.");
			return null;
		}
		
		//Initialize the distances for all nodes of the graph to infinity
		HashMap<GraphNode, Double> distances = new HashMap<GraphNode, Double>(); 
		this.nodes.forEach((k, v) -> {
			distances.put(v, Double.POSITIVE_INFINITY);
		});
		distances.replace(gnstart, (double) 0);
		
		HashSet<GraphNode> visited = new HashSet<GraphNode>();
		
		//Prepare the priority queue - each node will be sorted according to its corresponding distance from
		//gnstart. The distances are only saved in the distances HashMap, initialized above
		PriorityQueue<GraphNode> toExplore = new PriorityQueue<GraphNode>(new Comparator<GraphNode>(){
			public int compare(GraphNode gn1, GraphNode gn2) {
				return distances.get(gn1) == distances.get(gn2) ? 0 : 
					(distances.get(gn1) < distances.get(gn2) ? -1 : 1); 
			}
		});
		
		HashMap<GraphNode, GraphNode> parentMap = new HashMap<GraphNode, GraphNode>();
		
		int countNodes = 0;
		toExplore.add(gnstart);
		boolean found = false;
		while (!toExplore.isEmpty()) {
			GraphNode curr = toExplore.remove();
			countNodes++;
			if (!visited.contains(curr)) {
				visited.add(curr);
				// Hook for visualization.  See writeup.
				nodeSearched.accept(curr.getLocation());
				
				if (curr == gngoal) {
					found = true;
					break;
				}
				List<GraphNode> neighbors = curr.getNeighbors();
				ListIterator<GraphNode> it = neighbors.listIterator(neighbors.size());
				while (it.hasPrevious()) {
					GraphNode next = it.previous();
	
					if (!visited.contains(next)) {
						if (distances.get(next) > curr.getLocation().distance(next.getLocation()) + distances.get(curr)) {
							if (parentMap.containsKey(next)) {
								parentMap.replace(next, curr);
							}
							else {
								parentMap.put(next, curr);
							}
							distances.replace(next, curr.getLocation().distance(next.getLocation()) + distances.get(curr));
							toExplore.add(next);
						}
						
					}
				}
			}
		}
		
		if (!found) {
			System.out.println("No path exists");
			return null;
		}
		
		System.out.println("Nodes counted by Dijkstra: " + Integer.toString(countNodes));
		
		// return the reconstructed path
		return this.reconstructPath(gnstart, gngoal, parentMap);
		
		
	}
	
	/** Find the path from start to goal using A-Star search
	 * 
	 * @param start The starting location
	 * @param goal The goal location
	 * @return The list of intersections that form the shortest path from 
	 *   start to goal (including both start and goal).
	 */
	public List<GeographicPoint> aStarSearch(GeographicPoint start, GeographicPoint goal) {
		// Dummy variable for calling the search algorithms
        Consumer<GeographicPoint> temp = (x) -> {};
        return aStarSearch(start, goal, temp);
	}
	
	/** Find the path from start to goal using A-Star search
	 * 
	 * @param start The starting location
	 * @param goal The goal location
	 * @param nodeSearched A hook for visualization.  See assignment instructions for how to use it.
	 * @return The list of intersections that form the shortest path from 
	 *   start to goal (including both start and goal).
	 */
	public List<GeographicPoint> aStarSearch(GeographicPoint start, 
											 GeographicPoint goal, Consumer<GeographicPoint> nodeSearched)
	{
		// TODO: Implement this method in WEEK 3
		
		//Define the gnstart and gngoal or break
		GraphNode gnstart = this.nodes.get(start);
		GraphNode gngoal = this.nodes.get(goal);
		if ( gnstart == null || gngoal == null ) {
			System.out.println("Start or goal node is null!  No path exists.");
			return null;
		}
		
		//Initialize the distances for all nodes of the graph to infinity
		HashMap<GraphNode, Double> distances = new HashMap<GraphNode, Double>(); 
		this.nodes.forEach((k, v) -> {
			distances.put(v, Double.POSITIVE_INFINITY);
		});
		distances.replace(gnstart, (double) 0);
		
		//Initialize the theoretical distances for all nodes of the graph to infinity
		HashMap<GraphNode, Double> straightDistances = new HashMap<GraphNode, Double>(); 
		this.nodes.forEach((k, v) -> {
			straightDistances.put(v, gngoal.getLocation().distance(v.getLocation()));
		});
		straightDistances.replace(gnstart, (double) 0);
		
		/* Uncomment to print theoretical distances
		straightDistances.forEach( (k, v) -> {
			k.printNode();
			System.out.println("Theretical distance from goal node: " + Double.toString(v));
		});
		*/
		HashSet<GraphNode> visited = new HashSet<GraphNode>();
		
		//Prepare the priority queue - each node will be sorted according to its corresponding distance from
		//gnstart. The distances are only saved in the distances HashMap, initialized above
		PriorityQueue<GraphNode> toExplore = new PriorityQueue<GraphNode>(new Comparator<GraphNode>(){
			public int compare(GraphNode gn1, GraphNode gn2) {
				return distances.get(gn1) == distances.get(gn2) ? 0 : 
					(distances.get(gn1) < distances.get(gn2) ? -1 : 1); 
			}
		});
		
		HashMap<GraphNode, GraphNode> parentMap = new HashMap<GraphNode, GraphNode>();
		int countNodes = 0;
		toExplore.add(gnstart);
		boolean found = false;
		while (!toExplore.isEmpty()) {
			GraphNode curr = toExplore.remove();
			countNodes++;
			if (!visited.contains(curr)) {
				visited.add(curr);
				// Hook for visualization.  See writeup.
				nodeSearched.accept(curr.getLocation());
				
				if (curr == gngoal) {
					found = true;
					break;
				}
				List<GraphNode> neighbors = curr.getNeighbors();
				ListIterator<GraphNode> it = neighbors.listIterator(neighbors.size());
				while (it.hasPrevious()) {
					GraphNode next = it.previous();
	
					if (!visited.contains(next)) {
						if (distances.get(next) > curr.getLocation().distance(next.getLocation()) + distances.get(curr) + straightDistances.get(next)) {
						
							if (parentMap.containsKey(next)) {
								parentMap.replace(next, curr);
							}
							else {
								parentMap.put(next, curr);
							}
							distances.replace(next, curr.getLocation().distance(next.getLocation()) + distances.get(curr) + straightDistances.get(next));
							toExplore.add(next);
						}
						
					}
				}
			}
		}
		
		if (!found) {
			System.out.println("No path exists");
			return null;
		}
		
		System.out.println("Nodes counted by A*: " + Integer.toString(countNodes));
		
		// return the reconstructed path
		return this.reconstructPath(gnstart, gngoal, parentMap);
	}
	
	
	
	
	public static void main(String[] args)
	{
		/*
		System.out.print("Making a new map...");
		MapGraph theMap = new MapGraph();
		System.out.print("DONE. \nLoading the map...");
		GraphLoader.loadRoadMap("data/testdata/simpletest.map", theMap);
		System.out.println("DONE.");
		
		
		// My testing
		List<GeographicPoint> path = theMap.aStarSearch(new GeographicPoint(1, 1), new GeographicPoint(8, -1));
		//List<GeographicPoint> path = theMap.dijkstra(new GeographicPoint(1, 1), new GeographicPoint(8, -1));
		for (GeographicPoint gpn : path) {
			System.out.println(gpn.getX() + ", " + gpn.getY());
		}
		*/
		
		/*
		 * My testing
		theMap.printGraph();
		System.out.println("Neighbors of 4,1 node: \n");
		GraphNode gn = theMap.nodes.get(new GeographicPoint(4, 1));
		for (GraphNode gnn : gn.getNeighbors()) {
			System.out.println(gnn.getLocation().getX() + ", " + gnn.getLocation().getY());
		}
		//The loading of the graph is correct
		 */
		

		// You can use this method for testing.  
		
		///* Use this code in Week 3 End of Week Quiz
		MapGraph theMap = new MapGraph();
		System.out.print("DONE. \nLoading the map...");
		GraphLoader.loadRoadMap("data/maps/utc.map", theMap);
		System.out.println("DONE.");

		GeographicPoint start = new GeographicPoint(32.8648772, -117.2254046);
		GeographicPoint end = new GeographicPoint(32.8660691, -117.217393);
		
		
		List<GeographicPoint> route = theMap.dijkstra(start,end);
		List<GeographicPoint> route2 = theMap.aStarSearch(start,end);

		//*/
		
	}
	
}
