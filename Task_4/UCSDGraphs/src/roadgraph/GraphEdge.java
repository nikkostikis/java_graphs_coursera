/**
 * @author nikmac
 */
package roadgraph;

import geography.GeographicPoint;

/**
 * @author nikmac
 * 
 * This class represents a directed edge of the graph
 *
 */
public class GraphEdge {
	
	private GeographicPoint startLoc;
	private GeographicPoint endLoc;
	private String name;
	private String type;
	private double length;
	
	/**
	 * 
	 * Private constructor
	 * An edge can only be created through the nested builder class
	 */
	private GraphEdge(GeographicPoint sloc, GeographicPoint eloc, String n, String t, double l) {
		this.startLoc = sloc;
		this.endLoc = eloc;
		this.name = n;
		this.type = t;
		this.length = l;
	}
	
	/**
	 * @return the start point of the edge
	 */
	public GeographicPoint getStart() { 
		return new GeographicPoint(this.startLoc.getX(), this.startLoc.getY());
	}
	
	/**
	 * @return the end point of the edge
	 */
	public GeographicPoint getEnd() { 
		return new GeographicPoint(this.endLoc.getX(), this.endLoc.getY());
	}
	
	/**
	 * @return the name of the edge
	 */
	public String getName() { return this.name; }
	
	/**
	 * @return the type of the edge
	 */
	public String getType() { return this.type; }
	
	/**
	 * @return the length of the edge
	 */
	public double getLength() { return this.length; }
	
	/**
	 * Set the start point of the edge
	 */
	/*
	public void setStart(GeographicPoint gp) { this.startLoc = gp; }
	*/
	/**
	 * Set the end point of the edge
	 */
	/*
	public void setEnd(GeographicPoint gp) { this.endLoc = gp; }
	*/
	/**
	 * Set the name of the edge
	 */
	public void setName(String n) { this.name = n; }
	
	/**
	 * Set the type of the edge
	 */
	public void setType(String t) { this.type = t; }
	
	/**
	 * Set the length of the edge
	 */
	public void setLength(double l) { this.length = l; }
	
	
	/** 
	 * This nested class will be used to construct GraphEdges.
	 * It is an implementation of the "Builder" pattern to
	 * allow for optional parameters. It's static because all
	 * edges only need one builder.
	 */
	public static class GraphEdgeBuilder {
		
		private GeographicPoint _startLoc;
		private GeographicPoint _endLoc;
		private String _name;
		private String _type;
		private double _length;
		
		/** 
		 * Default public constructor
		 */
		public GraphEdgeBuilder() {	
			this._startLoc = new GeographicPoint(0, 0);
			this._endLoc = new GeographicPoint(0, 0);
			this._name = "None_Defined";
			this._type = "None_Defined";
			this._length = 0;
			
		}
		
		/** 
		 * Change the default name
		 */
		public GraphEdgeBuilder name(String n) { 
			this._name = n;
			return this;
		}
		
		/** 
		 * Change the default type
		 */
		public GraphEdgeBuilder type(String t) { 
			this._type = t;
			return this;
		}
		
		/** 
		 * Change the default length
		 */
		public GraphEdgeBuilder length(double l) { 
			this._length = l;
			return this;
		}
		
		/** 
		 * Change the default start location
		 */
		public GraphEdgeBuilder startLoc(GeographicPoint sloc) {
			this._startLoc = sloc;
			return this;
		}
		
		/** 
		 * Change the default end location
		 */
		public GraphEdgeBuilder endLoc(GeographicPoint eloc) {
			this._endLoc = eloc;
			return this;
		}
		
		/** 
		 * Create the GraphEdge instance with the builder's parameters
		 */
		public GraphEdge build() {
			return new GraphEdge(this._startLoc, this._endLoc, this._name, this._type, this._length);
		}
		
	}
	
}
